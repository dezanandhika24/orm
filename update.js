const { Article } = require('./models')

const whereClause = {
    where: { id: 1 }
}

// UPDATE FROM nama_table  SET nama_kolom = value_baru WHERE nama_kolom = nama_value
// UPDATE FROM Article  SET approved = false WHERE id = 1

Article.update({
    approved: false,
    title : "New Title"
}, whereClause).then(() => {
    console.log("Artikel berhasil diupdate ")
    process.exit()
  })
  .catch(err => {
    console.error("Gagal mengupdate artikel! ", err)
  })
 