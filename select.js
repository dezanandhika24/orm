const { Article } = require('./models')

const whereClause = {
    where: { id: 1 }
}

Article.findOne({
    where: { id: 2 }
}).then(article => {
    console.log(article)
})